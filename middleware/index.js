var Campground = require("../models/campground"),
    Comment    = require("../models/comment");

// All the middleare goes here
var middlewareObj = {};

middlewareObj.isLoggedIn = function(req, res, next) {
  if(req.isAuthenticated()){
    return next();
  }

  // key: 'error', val: 'Please login first'
  req.flash('error', 'Please login first');  /***** NEWLY ADDED *****/
  res.redirect("/login");
};

middlewareObj.checkCampgroundOwnership = function(req, res, next) {
    if (req.isAuthenticated()) {
      Campground.findById(req.params.id, function(err, foundCampground) {
        if (err) {
          res.redirect("back"); 
        }
        else {
          if (foundCampground.author.id.equals(req.user.id)) {
            next();
          } else {
            res.redirect("back");
          }
        }
      });
    } else {
      res.redirect("back"); 
    }
}

middlewareObj.checkCommentOwnership = function(req, res, next) {
  if(req.isAuthenticated()){  // Check if the user is logged in
    Comment.findById(req.params.comment_id, function(err, foundComment){
      if(err){
          res.redirect("back");
      } else {
        if(foundComment.author.id.equals(req.user._id)) {
          next();
        } else {
          res.redirect("back");
        }
      }
    });
  } else {
    res.redirect("back");
  }
}

module.exports = middlewareObj;
